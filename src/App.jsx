import TreeComp from './components/TreeComp';
import './App.css';

const App = () => {
  return (
    <section className='tree'>
      <p>Please upload a Json file: </p>
      <TreeComp />
    </section>
  );
}

export default App;
