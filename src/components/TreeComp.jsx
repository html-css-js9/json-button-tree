import React from 'react';
import { Tree, TreeNode } from 'react-organizational-chart';
import styled from 'styled-components';

const StyledNode = styled.div`
  padding: 5px;
  border-radius: 8px;
  display: inline-block;
  border: 1px solid red;
`;

class TreeComp extends React.Component {

  constructor(props) {
    super(props);
    this.state = { treeNodes: new Map(), treeRootNode: [], globalChildren: new Map(), rootName: "" };
  }

  createRootNode = (name, children) => {
    return (
      <div>
        <Tree
          lineWidth={'2px'}
          lineColor={'green'}
          lineBorderRadius={'10px'}
          label={<StyledNode>{name}</StyledNode>}
        >
          {children}
        </Tree>
      </div>
    );
  }

  createNode = (name, children) => {
    //let renderArray = [];
    //this.setState({this.state.globalChildren: children});
    return (
      <TreeNode key={name} label={<StyledNode>{name}</StyledNode>}>
          <button onClick={() => {this.addNodes(name, children)}}>Add Button</button>
          {console.log(this.state.globalChildren.get(name))}
          {this.state.globalChildren.get(name)}
      </TreeNode>
    );
  }

  getNodesHelper = (element) => {
    if (typeof element !== 'undefined') {
      const childrenNodes = [element.name];
      //console.log(element.name);
      if (typeof element.children === 'object') {
        element.children.forEach((child) => {
          childrenNodes.push(child.name);
          this.getNodesHelper(child);
        });
        this.state.treeNodes.set(childrenNodes[0], childrenNodes.slice(1));
      }
    }
  }

  getNodes = (jsonFile) => {
    Object.keys(jsonFile).forEach((element) => {
      this.getNodesHelper(jsonFile[element]);
    });
  }

  /*
  getNodes = (jsonFile) => {
    if (typeof jsonFile === 'object') {
      Object.keys(jsonFile).forEach((element) => {
        //console.log(jsonFile[element].name)
        //this.state.treeNodes.push(this.createNode(jsonFile[element].name));
        const childrenNodes = [jsonFile[element].name];
        if (typeof jsonFile[element].children === 'object') {
          jsonFile[element].children.forEach((child) => {
            //console.log(child.name)
            //this.state.treeNodes.push(this.createNode(child.name));
            childrenNodes.push(child.name);
            this.getNodes(child.children);
          });
          this.state.treeNodes.push(childrenNodes);
        }
        this.state.treeNodes.push(childrenNodes);
      });
    }
  }
  */

  loadFile = (event) => {
    const input_file = event.target;
    const reader = new FileReader();
    reader.readAsText(input_file.files[0]);
    reader.onload = () => {
      const jsonObject = JSON.parse(reader.result);
      //const jsonMap = new Map(Object.entries(jsonObject));
      console.log(jsonObject);
      this.getNodes(jsonObject);
      this.setState({ treeNodes: this.state.treeNodes });
      console.log(this.state.treeNodes);
      this.state.rootName = jsonObject[0].name;
      this.setState({ rootName: this.state.rootName });
      this.addToRoot(this.state.rootName);
      this.setState({ treeRootNode: this.state.treeRootNode });
      //this.createTree();
    }
  }

  addToRoot = (name) => {
    const rootChildren = this.state.treeNodes.get(name);
    const insertChildren = [];
    rootChildren.forEach((child) => {
      insertChildren.push(this.createNode(child, []));
    });
    this.state.treeRootNode.push(this.createRootNode(name, insertChildren));
  }

  addNodes = (name, children) => {
    if (this.state.treeNodes.has(name)) {
      const childrenName = this.state.treeNodes.get(name);
      childrenName.forEach((child) => {
        children.push(this.createNode(child, []));
      });
    }
    //console.log(name + ": " + children);
    this.state.globalChildren.set(name, children);
    this.setState({ globalChildren: this.state.globalChildren });
    this.state.treeRootNode = [];
    this.setState({ treeRootNode: this.state.treeRootNode });
    this.addToRoot(this.state.rootName);
    this.setState({ treeRootNode: this.state.treeRootNode });
  }

  render() {
    return (
      <section>
        <div>
          <input type='file' accept='Json' onChange={(e) => this.loadFile(e)} />
        </div>
        <div>
          <div>
            {this.state.treeRootNode}
          </div>
        </div>
      </section>
    );
  }
}

export default TreeComp;